from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Unversitetlar(models.Model):
    nomi = models.CharField(max_length=200, blank=True, null=True)
    qisqa_nomi = models.CharField(max_length=20)

    def __str__(self):
        return self.nomi

class Fakultetlar(models.Model):
    nomi = models.CharField(max_length=200, blank=True, null=True)
    unversitet = models.ForeignKey(Unversitetlar, on_delete=models.CASCADE)

    def __str__(self):
        return self.nomi

class Yunalishlar(models.Model):
    nomi = models.CharField(max_length=200, blank=True, null=True)
    fakultet = models.ForeignKey(Fakultetlar, on_delete=models.CASCADE)

    def __str__(self):
        return self.nomi

class Guruhlar(models.Model):
    nomi = models.CharField(max_length=200, blank=True, null=True)
    yunalish = models.ForeignKey(Yunalishlar, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nomi

class Bulimlar(models.Model):
    nomi = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return self.nomi
class Narxlar(models.Model):
    narx = models.FloatField(blank=True, null=True)
    tel_raqam = models.IntegerField()
    plastik_raqam = models.IntegerField(blank=True, null=True)
    plastik_fish = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField()
    

class Mavzular(models.Model):
    tipi_choices = [
        ('new', 'new'),
        ('old', 'old'),
        ('pay', 'pay')
    ]
    nomi = models.CharField(max_length=200, blank=True, null=True)
    savol_soni = models.IntegerField(blank=True, null=True)
    tipi = models.CharField(max_length=10, choices=tipi_choices, default='new')
    narx = models.ForeignKey(Narxlar, on_delete=models.CASCADE)
    bulim = models.ForeignKey(Bulimlar, on_delete=models.CASCADE)

    def __str__(self):
        return self.nomi

class UqishUchunMatnlar(models.Model):
    matn = models.TextField()
    bulim = models.ForeignKey(Bulimlar, on_delete=models.CASCADE)

    def __str__(self):
        return self.matn

class Savollar(models.Model):
    matn = models.CharField(max_length=255, blank=True, null=True)
    mavzu = models.ForeignKey(Mavzular, on_delete=models.CASCADE)
    belgilangan = models.BooleanField()

    def __str__(self):
        return self.matn

class QismiySavollar(models.Model):
    matn = models.CharField(max_length=255, blank=True, null=True)
    savol = models.ForeignKey(Savollar, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.matn

class Lugatlar(models.Model):
    matn = models.CharField(max_length=255, blank=True, null=True)
    savol = models.ForeignKey(Savollar, on_delete=models.CASCADE)

    def __str__(self):
        return self.matn

class Goyalar(models.Model):
    matn = models.CharField(max_length=255, blank=True, null=True)
    savol = models.ForeignKey(Savollar, on_delete=models.CASCADE)

    def __str__(self):
        return self.matn

class Javoblar(models.Model):
    matn = models.CharField(max_length=255, blank=True, null=True)
    savol = models.ForeignKey(Savollar, on_delete=models.CASCADE)

    def __str__(self):
        return self.matn

class MalumotlarInfo(models.Model):
    savol = models.CharField(max_length=255, blank=True, null=True)
    javob = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.savol} - {self.javob}'

class Foydalanuvchilar(models.Model):
    tipi_choices = [
        ('admin', 'admin'),
        ('manager', 'manager'),
        ('teacher', 'teacher'),
        ('student', 'student'),
        ('guest', 'guest'),
    ]
    ism = models.CharField(max_length=25)
    familiya = models.CharField(max_length=25)
    sharif = models.CharField(max_length=25, blank=True, null=True)
    tel_raqam = models.IntegerField()
    email = models.EmailField(blank=True, null=True)
    tipi = models.CharField(max_length=10, choices=tipi_choices, default='manager')
    guruh = models.ForeignKey(Guruhlar, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.ism} {self.familiya}'

class Tulovlar(models.Model):
    narx = models.ForeignKey(Narxlar, on_delete=models.CASCADE)
    tulangan_summa = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tulov_sana = models.DateField()
    qayd_sana = models.DateField(auto_now_add=True) 

    def __str__(self):
        return float(self.tulangan_summa)

class Jurnal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sana = models.DateField()
    vaqt = models.TimeField(blank=True, null=True)

class Bajarilganlar(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    savol = models.ForeignKey(Savollar, on_delete=models.CASCADE)
    savol_bajarilgan = models.FloatField()
    lugat_bajarilgan = models.FloatField()
    goya_bajarilgan = models.FloatField()
    urtacha = models.FloatField()

class Buyurtmalar(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    narx = models.ForeignKey(Narxlar, on_delete=models.CASCADE)
    narx = models.FloatField()
    sana = models.DateField()
    vaqt = models.TimeField(blank=True, null=True)

