# Generated by Django 3.1.2 on 2020-10-25 11:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bajarilganlar',
            old_name='savol_id',
            new_name='savol',
        ),
        migrations.RenameField(
            model_name='bajarilganlar',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='buyurtmalar',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='fakultetlar',
            old_name='unversitet_id',
            new_name='unversitet',
        ),
        migrations.RenameField(
            model_name='foydalanuvchilar',
            old_name='guruh_id',
            new_name='guruh',
        ),
        migrations.RenameField(
            model_name='goyalar',
            old_name='savol_id',
            new_name='savol',
        ),
        migrations.RenameField(
            model_name='guruhlar',
            old_name='yunalish_id',
            new_name='yunalish',
        ),
        migrations.RenameField(
            model_name='javoblar',
            old_name='savol_id',
            new_name='savol',
        ),
        migrations.RenameField(
            model_name='jurnal',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='lugatlar',
            old_name='savol_id',
            new_name='savol',
        ),
        migrations.RenameField(
            model_name='mavzular',
            old_name='bulim_id',
            new_name='bulim',
        ),
        migrations.RenameField(
            model_name='mavzular',
            old_name='narx_id',
            new_name='narx',
        ),
        migrations.RenameField(
            model_name='qismiysavollar',
            old_name='savol_id',
            new_name='savol',
        ),
        migrations.RenameField(
            model_name='savollar',
            old_name='mavzu_id',
            new_name='mavzu',
        ),
        migrations.RenameField(
            model_name='tulovlar',
            old_name='narx_id',
            new_name='narx',
        ),
        migrations.RenameField(
            model_name='tulovlar',
            old_name='user_id',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='uqishuchunmatnlar',
            old_name='bulim_id',
            new_name='bulim',
        ),
        migrations.RenameField(
            model_name='yunalishlar',
            old_name='fakultet_id',
            new_name='fakultet',
        ),
        migrations.RemoveField(
            model_name='buyurtmalar',
            name='narx_id',
        ),
    ]
