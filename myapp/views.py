from django.shortcuts import render,get_object_or_404, redirect
from .models import (
                    Bajarilganlar, Bulimlar, Buyurtmalar, Fakultetlar, Foydalanuvchilar, Goyalar, 
                    Guruhlar, Javoblar, Jurnal, Lugatlar, MalumotlarInfo, Mavzular, Narxlar, 
                    QismiySavollar, Savollar, Tulovlar, Unversitetlar, UqishUchunMatnlar, Yunalishlar )

from django.contrib.auth.decorators import login_required

# Create your views here.

def home(request):
    bulimlar = Bulimlar.objects.all()
    context = {
        'bulimlar': bulimlar,
    }
    return render(request, 'myapp/home.html', context)

@login_required
def part1(request):   
    mavzular = Mavzular.objects.filter(bulim__id = 1)
    context = {
        'mavzular': mavzular,
    }
    
    return render(request, 'myapp/part1.html', context)

@login_required
def part2(request):
    mavzular = Mavzular.objects.filter(bulim__id = 2)
    context = {
        'mavzular': mavzular,        
    }
    return render(request, 'myapp/part2.html', context)

@login_required
def part3(request):
    mavzular = Mavzular.objects.filter(bulim__id = 3)
    context = {
        'mavzular': mavzular,        
    }
    return render(request, 'myapp/part3.html', context)

@login_required
def bookmarks(request):
    return render(request, 'myapp/bookmarks.html')

@login_required
def buy(request):
    return render(request, 'myapp/buy.html')

@login_required
def info(request):
    return render(request, 'myapp/info.html')

@login_required
def matnlar1(request):
    matnlar = UqishUchunMatnlar.objects.filter(bulim__id = 1)

    context = {
        'matnlar': matnlar,
    }
    return render(request, 'myapp/matnlar1.html', context)

@login_required
def matnlar2(request):
    matnlar = UqishUchunMatnlar.objects.filter(bulim__id = 2)

    context = {
        'matnlar': matnlar,
    }
    return render(request, 'myapp/matnlar2.html', context)

@login_required
def matnlar3(request):
    matnlar = UqishUchunMatnlar.objects.filter(bulim__id = 3)

    context = {
        'matnlar': matnlar,
    }
    return render(request, 'myapp/matnlar3.html', context)

def savollar(request, pk):
    savollar = Savollar.objects.filter(mavzu__id = pk)
    qismiy_savollar = QismiySavollar.objects.filter(savol__id = pk)
    

    context = {
        'savollar': savollar,
        'qismiy_savollar': qismiy_savollar
    }
    return render(request, 'myapp/savollar.html', context)

def savollar_pay(request):
    return render(request, 'myapp/savollar_pay.html')

def javoblar(request, pk):
    savol = get_object_or_404(Savollar, id=pk)
    lugatlar = Lugatlar.objects.filter(savol__id = pk)
    goyalar = Goyalar.objects.filter(savol__id = pk)
    javoblar = Javoblar.objects.filter(savol__id = pk)

    context = {
        'savol': savol,
        'lugatlar': lugatlar,
        'goyalar': goyalar,
        'javoblar': javoblar,
    }
    return render(request, 'myapp/javoblar.html', context)





