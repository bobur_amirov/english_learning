from django.urls import path
from .views import (home, part1, part2, part3, bookmarks, buy, info, matnlar1, matnlar2, matnlar3, savollar,
                    savollar_pay, javoblar )

urlpatterns = [
    path('', home, name='home'),
    path('part1/', part1, name='part1'),
    path('part2/', part2, name='part2'),
    path('part3/', part3, name='part3'),
    path('bookmarks/', bookmarks, name='bookmarks'),
    path('buy/', buy, name='buy'),
    path('info/', info, name='info'),
    path('matnlar1/', matnlar1, name='matnlar1'),
    path('matnlar2/', matnlar2, name='matnlar2'),
    path('matnlar3/', matnlar3, name='matnlar3'),
    path('savol/<int:pk>/', savollar, name='savollar'),
    path('javob/<int:pk>/', javoblar, name='javoblar'),
    path('savollar-pay/', savollar_pay, name='savollar_pay'),
    

]