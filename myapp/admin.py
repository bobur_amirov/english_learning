from django.contrib import admin
from .models import (
                    Bajarilganlar, Bulimlar, Buyurtmalar, Fakultetlar, Foydalanuvchilar, Goyalar, 
                    Guruhlar, Javoblar, Jurnal, Lugatlar, MalumotlarInfo, Mavzular, Narxlar, 
                    QismiySavollar, Savollar, Tulovlar, Unversitetlar, UqishUchunMatnlar, Yunalishlar )

# Register your models here.
admin.site.register(Bajarilganlar)
admin.site.register(Bulimlar)
admin.site.register(Buyurtmalar)
admin.site.register(Fakultetlar)
admin.site.register(Foydalanuvchilar)
admin.site.register(Goyalar)
admin.site.register(Guruhlar)
admin.site.register(Javoblar)
admin.site.register(Jurnal)
admin.site.register(Lugatlar)
admin.site.register(MalumotlarInfo)
admin.site.register(Mavzular)
admin.site.register(Narxlar)
admin.site.register(QismiySavollar)
admin.site.register(Savollar)
admin.site.register(Tulovlar)
admin.site.register(Unversitetlar)
admin.site.register(UqishUchunMatnlar)
admin.site.register(Yunalishlar)